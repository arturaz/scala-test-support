package support_shared.helpers

import support.TestSpec


class WrappedDescribeTest extends TestSpec with WrappedDescribe
{
  var stack = List.empty[String]

  val (outer, inner1, inner2, inner3) = ("outer", "inner1", "inner2", "inner3")

  wrappedDescribe(outer)(
    before = { stack ::= s"$outer before" },
    around = { fun =>
      stack ::= s"$outer a-start"; fun(); stack ::= s"$outer a-end"
    },
    after = { stack ::= s"$outer after" }
  ) { it =>
    wrappedDescribe(inner1, it)(
      before = { stack ::= s"$inner1 before" },
      around = { fun =>
        stack ::= s"$inner1 a-start"
        fun()
        stack ::= s"$inner1 a-end"
      },
      after = { stack ::= s"$inner1 after" }
    ) { it =>
      wrappedDescribe(inner2, it)(
        before = { stack ::= s"$inner2 before" },
        around = { fun =>
          stack ::= s"$inner2 a-start"
          fun()
          stack ::= s"$inner2 a-end"
        },
        after = { stack ::= s"$inner2 after" }
      ) { it =>
        wrappedDescribe(inner3, it)(
          before = { stack ::= s"$inner3 before" },
          around = { fun =>
            stack ::= s"$inner3 a-start"
            fun()
            stack ::= s"$inner3 a-end"
          },
          after = { stack ::= s"$inner3 after" }
        ) { it =>
          it("should have a proper inwards structure in the stack") {
            stack should === (
              s"$inner3 a-start" :: s"$inner3 before" ::
              s"$inner2 a-start" :: s"$inner2 before" ::
              s"$inner1 a-start" :: s"$inner1 before" ::
              s"$outer a-start"  :: s"$outer before" ::
              Nil
            )
            stack ::= "test"
          }
        }
      }
    }
  }

  it("should have a proper complete structure in the stack") {
    stack should === (
      s"$outer after"  :: s"$outer a-end" ::
      s"$inner1 after" :: s"$inner1 a-end" ::
      s"$inner2 after" :: s"$inner2 a-end" ::
      s"$inner3 after" :: s"$inner3 a-end" ::
      "test" ::
      s"$inner3 a-start" :: s"$inner3 before" ::
      s"$inner2 a-start" :: s"$inner2 before" ::
      s"$inner1 a-start" :: s"$inner1 before" ::
      s"$outer a-start"  :: s"$outer before" ::
      Nil
    )
  }
}
