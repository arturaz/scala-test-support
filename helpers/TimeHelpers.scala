package support_shared.helpers

import org.joda.time.{DateTimeZone, DateTime}

trait TimeHelpers {
  def nowSec = System.currentTimeMillis / 1000
  // We must specify a generic timezone, otherwise this get wicked when
  // serializing and then parsing same values to/from JSON.
  def nowDateTime = new DateTime().withZone(DateTimeZone.UTC)
}
