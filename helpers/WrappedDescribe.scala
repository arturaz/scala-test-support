package support_shared.helpers

import org.scalatest.{FunSpecLike, Tag}

/**
 * Allows you to use RSpec style before/after/around side-effects on examples.
 *
 * TODO: write a blog post about this.
 */
trait WrappedDescribe { self: FunSpecLike =>
  type TestFunction = () => Unit

  /**
   * Describe with code executed before, around and after examples.
   *
   * @param before code that is executed before example
   * @param after code that is executed after example
   * @param around code that is executed around example
   * @param fun block that holds actual examples
   * @param outerWrappedIt optional wrapped it word obtained from other
   *                       wrapped describe
   */
  def wrappedDescribe(
    description: String, outerWrappedIt: WrappedItWord = NullWrappedItWord
  )(
    before: => Unit = (),
    after: => Unit = (),
    around: TestFunction => Unit = { _() }
  )(fun: WrappedItWord => Unit): Unit = {
    describe(description) {
      fun(new WrappedItWord(
        before = () => before, after = () => after, around = around,
        outerWrappedIt = outerWrappedIt
      ))
    }
  }

  protected def wrappedIt(
    before: => Unit = (),
    after: => Unit = (),
    around: TestFunction => Unit = { _() },
    outerWrappedIt: WrappedItWord = NullWrappedItWord
  ) = new WrappedItWord(
    before = () => before, after = () => after, around = around,
    outerWrappedIt = outerWrappedIt
  )

  object WrappedItWord {
    def run(start: WrappedItWord, testFun: => Unit): Unit = {
      var stack = List.empty[WrappedItWord]

      def buildStack(current: WrappedItWord): Unit = {
        stack ::= current
        val parent = current.outerWrappedIt
        if (! parent.isRoot) buildStack(parent)
      }
      buildStack(start)

      def executeStack(currentStack: List[WrappedItWord]): Unit = {
        val current :: tail = currentStack
        current.before()
        current.around { () => tail match {
          case Nil => testFun
          case _ => executeStack(tail)
        } }
        current.after()
      }
      executeStack(stack)
    }
  }

  protected class WrappedItWord(
    protected val before: () => Unit,
    protected val after: () => Unit,
    protected val around: TestFunction => Unit,
    protected val outerWrappedIt: WrappedItWord
  ) extends ItWord {
    protected val isRoot = false

    override def apply(specText: String, testTags: Tag*)(testFun: => Unit): Unit = {
      super.apply(specText, testTags:_*) { WrappedItWord.run(this, testFun) }
    }
  }

  // Represents "wrapped" it word which is not actually wrapped.
  protected object NullWrappedItWord extends WrappedItWord(
    before = () => (),
    after = () => (),
    around = f => f(),
    outerWrappedIt = null
  ) {
    override protected val isRoot = true
  }
}
