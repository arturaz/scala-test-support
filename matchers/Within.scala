package support_shared.matchers

import org.hamcrest.{TypeSafeMatcher, Description}
import org.joda.time.DateTime

/**
 * Created with IntelliJ IDEA.
 * User: arturas
 * Date: 11/26/12
 * Time: 6:58 PM
 * To change this template use File | Settings | File Templates.
 */
class Within[T](instant: DateTime, millis: Int, transform: T => DateTime)
extends TypeSafeMatcher[T] {
  def matchesSafely(o: T) = {
    val dateTime = transform(o)
    math.abs(dateTime.getMillis - instant.getMillis) <= millis
  }

  def describeTo(description: Description): Unit = {
    description.appendText(s"value within $millis millis of $instant")
  }
}
