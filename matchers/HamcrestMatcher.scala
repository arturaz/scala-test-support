package support_shared.matchers

import org.hamcrest.{Description, TypeSafeMatcher}

class HamcrestMatcher[T](errorDescription: String, matches: T => Boolean)
extends TypeSafeMatcher[T] {
  def matchesSafely(obj: T) = matches(obj)

  def describeTo(description: Description): Unit = {
    description.appendText(errorDescription)
  }
}
