package support_shared.matchers

import org.scalatest.matchers.{MatchResult, Matcher}
import reflect.ClassTag

/**
 * Created with IntelliJ IDEA.
 * User: arturas
 * Date: 2/15/13
 * Time: 1:59 PM
 * To change this template use File | Settings | File Templates.
 */
class BeInstanceOf[T : ClassTag] extends Matcher[Any] {
  private[this] val classTag = implicitly[ClassTag[T]]

  def apply(left: Any) = {
    MatchResult(
      classTag.unapply(left).isDefined,
      s"Expected $left to be ${classTag.runtimeClass.getCanonicalName
        }, but it was ${left.getClass.getCanonicalName}",
      s"Expected $left not to be ${classTag.runtimeClass.getCanonicalName
        }, but it was."
    )
  }
}
