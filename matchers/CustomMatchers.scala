package support_shared.matchers

import reflect.ClassTag
import org.mockito.{Matchers => M}
import org.scalatest.Matchers

/**
 * Created with IntelliJ IDEA.
 * User: arturas
 * Date: 9/18/12
 * Time: 6:23 PM
 * To change this template use File | Settings | File Templates.
 */
trait CustomMatchers extends Matchers {
  /**
   * For lambda should change matching, i.e.:
   * <code>
   *   lambda { foo() } should change(bar).by(3)
   * </code>
   *
   * @param code
   * @return
   */
  def lambda(code: => Unit) = () => code

  /**
   * Change matching, i.e.:
   * <code>
   *   lambda { obj.stuff() } should change(obj.var)
   *   lambda { obj.stuff() } should change(obj.var).from(10).to(15)
   *   lambda { obj.stuff() } should change(obj.var).by(12)
   *   lambda { obj.stuff() } should not (change(obj.var))
   * </code>
   *
   * @param property
   * @tparam T
   * @return
   */
  def change[T](property: => T) = new ChangeMatcher(() => property)

  val beEmpty = new BeEmpty
  def beInstanceOf[T : ClassTag] = new BeInstanceOf[T]

  def hamcrestMatcher[T](errorDescription: String="")(matches: T => Boolean) =
    M.argThat(new HamcrestMatcher(errorDescription, matches))

  /**
   * Imperative hamcrest matcher. Expects to have some kind of inner checking
   * in `matches`.
   **/
  def hamcrestIMatcher[T](matches: T => Any) = {
    val imperative = (o: T) => { matches(o); true }
    M.argThat(new HamcrestMatcher[T]("", imperative))
  }

  def beBoolean(b: Boolean) = new BeBoolean(b)
  val beTrue = beBoolean(true)
  val beFalse = beBoolean(false)

  /**
   * Mockito provides spies for real objects, but if the spy method is called
   * with wrong arguments, it just falls through to real objects. Which sucks,
   * because we're testing whether the object was called with the right args
   * in the first place.
   *
   * Usage:
   *
   *   expectingCall((1, 2, 3)) { register =>
   *     new MyObj {
   *       def doStuff(a: Int, b: Int, c: Int) = {
   *         register(a, b, c)
   *         super.doStuff(a, b, c)
   *       }
   *     }.doStuff()
   *   }
   *
   * @param expected arguments with which we are expecting a call
   * @param callRegistrator code block that receives registration function
   *                        which you call to register that the code was called
   *                        with given arguments
   * @return return value from f
   */
  def expectingCall[Args, Return]
  (expected: Args)(callRegistrator: CallRegistrator[Args] => Return): Return = {
    var called = false
    val returnValue = callRegistrator { actual =>
      called = true
      actual should === (expected)
    }
    called should beTrue
    returnValue
  }

  type CallRegistrator[Args] = Args => Any
}

object CustomMatchers extends CustomMatchers
