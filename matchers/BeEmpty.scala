package support_shared.matchers

import org.scalatest.matchers.{MatchResult, Matcher}

/**
 * Created with IntelliJ IDEA.
 * User: arturas
 * Date: 4/24/13
 * Time: 3:44 PM
 * To change this template use File | Settings | File Templates.
 */
class BeEmpty extends Matcher[TraversableOnce[_]] {
  def apply(left: TraversableOnce[_]) = {
    MatchResult(
      left.isEmpty,
      s"$left should have been empty, but was not.",
      s"$left should have not been empty, but it was."
    )
  }
}
