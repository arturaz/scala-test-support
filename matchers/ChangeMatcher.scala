package support_shared.matchers

import org.scalatest.matchers.{MatchResult, Matcher}

/**
 * Created with IntelliJ IDEA.
 * User: arturas
 * Date: 9/18/12
 * Time: 6:13 PM
 * To change this template use File | Settings | File Templates.
 */
object BaseChangeMatcher {
  type Lambda = () => Unit
}

trait BaseChangeMatcher[T] extends Matcher[BaseChangeMatcher.Lambda] {
  protected val getProperty: () => T
  lazy protected val initialValue = getProperty()
  lazy protected val afterValue = getProperty()

  private[this] val ShouldHaveChanged =
    "Value should have changed from %s but it did not."
  private[this] val ShouldHaveNotChanged =
    "Value should have not changed from %s but it did to %s."

  def apply(lambda: BaseChangeMatcher.Lambda): MatchResult = {
    initialValue
    lambda.apply()
    afterValue

    MatchResult(
      initialValue != afterValue,
      ShouldHaveChanged.format(initialValue),
      ShouldHaveNotChanged.format(initialValue, afterValue),
      ShouldHaveChanged.toLowerCase.format(initialValue),
      ShouldHaveNotChanged.toLowerCase.format(initialValue, afterValue)
    )
  }
}

class ChangeMatcher[T](
  protected val getProperty: () => T
) extends BaseChangeMatcher[T] {
  import NumericChangeMatcher._

  // Turn into absolute change matcher with from condition set.
  def from(value: T) =
    new AbsoluteChangeMatcher(getProperty, Some(value), None)

  // Turn into absolute change matcher with to condition set.
  def to(value: T) =
    new AbsoluteChangeMatcher(getProperty, None, Some(value))

  // Turn into relative change matcher with change set.
  def by[A >: T](change: A)(implicit numeric: Numeric[A]) =
    new NumericChangeMatcher(getProperty, numeric, Exactly(change))

  def atLeastBy[A >: T](value: A)(implicit numeric: Numeric[A]) =
    new NumericChangeMatcher(getProperty, numeric, AtLeast(value))

  def atMostBy[A >: T](value: A)(implicit numeric: Numeric[A]) =
    new NumericChangeMatcher(getProperty, numeric, AtMost(value))
}

class AbsoluteChangeMatcher[T](
  protected val getProperty: () => T,
  expectedInitialValue: Option[T]=None,
  expectedAfterValue: Option[T]=None
) extends BaseChangeMatcher[T] {
  private[this] val InitialFormat =
    "Initial value expected to be %s but it was %s."
  private[this] val AfterFormat =
    "After value expected to be %s but it was %s."
  private[this] val NotUsable = "Matcher not usable in this way!"

  def from(value: T) =
    new AbsoluteChangeMatcher(getProperty, Some(value), expectedAfterValue)
  def to(value: T) =
    new AbsoluteChangeMatcher(getProperty, expectedInitialValue, Some(value))

  override def apply(lambda: BaseChangeMatcher.Lambda): MatchResult = {
    val result = super.apply(lambda)

    expectedInitialValue match {
      case None => ()
      case Some(expected) =>
        if (initialValue != expected)
          return failure(InitialFormat, expected, initialValue)
    }

    expectedAfterValue match {
      case None => ()
      case Some(expected) =>
        if (afterValue != expected)
          return failure(AfterFormat, expected, afterValue)
    }

    result
  }

  private[this] def failure(format: String, args: Any*) = {
    MatchResult(
      matches = false,
      format.format(args:_*), NotUsable,
      format.toLowerCase.format(args:_*), NotUsable
    )
  }
}

object NumericChangeMatcher {
  sealed trait Change[T] {
    def wrongChangeMsg(actualChange: T): String
    def shouldNotHaveChanged(actualChange: T): String
    def matches(actualChange: T): Boolean
  }

  case class Exactly[T : Numeric](value: T) extends Change[T] {
    def wrongChangeMsg(actualChange: T) =
      s"Value should have changed by $value but it did change by $actualChange."

    def shouldNotHaveChanged(actualChange: T) =
      s"Value should have not changed by $value but it did change."

    def matches(actualChange: T) = actualChange == value
  }

  case class AtLeast[T : Numeric](value: T) extends Change[T] {
    def wrongChangeMsg(actualChange: T) =
      s"Value should have changed by at least $value but it did only change " +
      s"by $actualChange."

    def shouldNotHaveChanged(actualChange: T) =
      s"Value should have not changed by at least $value but it did change " +
      s"by $actualChange."

    def matches(actualChange: T) =
      implicitly[Numeric[T]].compare(actualChange, value) >= 0
  }

  case class AtMost[T : Numeric](value: T) extends Change[T] {
    def wrongChangeMsg(actualChange: T) =
      s"Value should have changed by at most $value but it did change " +
      s"by $actualChange."

    def shouldNotHaveChanged(actualChange: T) =
      s"Value should have not changed by at most $value but it did change " +
      s"by $actualChange."

    def matches(actualChange: T) =
      implicitly[Numeric[T]].compare(actualChange, value) <= 0
  }
}

class NumericChangeMatcher[T](
  protected val getProperty: () => T,
  numeric: Numeric[T], expectedChange: NumericChangeMatcher.Change[T]
) extends BaseChangeMatcher[T] {
  override def apply(lambda: BaseChangeMatcher.Lambda): MatchResult = {
    super.apply(lambda)
    val actualChange = numeric.minus(afterValue, initialValue)

    MatchResult(
      expectedChange.matches(actualChange),
      expectedChange.wrongChangeMsg(actualChange),
      expectedChange.shouldNotHaveChanged(actualChange),
      expectedChange.wrongChangeMsg(actualChange).toLowerCase,
      expectedChange.shouldNotHaveChanged(actualChange).toLowerCase
    )
  }
}